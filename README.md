# Kafka on Docker

## Start brokers

```
./setup.sh start <number_of_brokers>
```

## Stop brokers

```
./setup.sh stop <number_of_brokers>
```

## Start consumer

```
./setup.sh consumer <topic_name>
```

## Start producer

```
./setup.sh producer <topic_name>
```

## New topic

```
./setup.sh topic <topic_name> <partitions>
```

