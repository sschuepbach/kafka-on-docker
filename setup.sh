#/bin/sh

# SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

case "$1" in
    "start")
        if ! `docker network ls | cut -d' ' -f9 | grep -q "^kafka$"`; then
            echo "Creating network `kafka`"
            docker network create kafka
        fi

        docker run --rm -d --name zookeeper --net=kafka --expose 2181 zookeeper

        for i in $(seq 1 $2); do 
            echo "Creating Kafka instance kafka-$i"
            docker run -d \
                --rm \
                --name=kafka-$i \
                --entrypoint bin/kafka-server-start.sh \
                --net=kafka \
                --expose 7203 \
                -p$((9091 + $i)):9092 \
                solsson/kafka \
                config/server.properties \
                --override zookeeper.connect=zookeeper:2181 \
                --override broker.id=$i \
                --override advertised.listeners=INSIDE://kafka-$i:9091,OUTSIDE://localhost:$((9091 + $i)) \
                --override listener.security.protocol.map=INSIDE:PLAINTEXT,OUTSIDE:PLAINTEXT \
                --override listeners=INSIDE://0.0.0.0:9091,OUTSIDE://0.0.0.0:9092 \
                --override inter.broker.listener.name=INSIDE
        done
        ;;

    "stop")
        for i in $(seq 1 $2); do
            echo "Shutting down Kafka instance kafka-$i"
            docker stop kafka-$i
        done
        echo "Shutting down Zookeeper instance"
        docker stop zookeeper
        ;;

    "consumer")
        docker run --rm --entrypoint bin/kafka-console-consumer.sh --net=kafka solsson/kafka --bootstrap-server kafka-1:9091,kafka-2:9091,kafka-3:9091 --topic $2 --from-beginning
        ;;

    "producer")
        docker run -it --rm --entrypoint bin/kafka-console-producer.sh --net=kafka solsson/kafka --broker-list kafka-1:9091,kafka-2:9091,kafka-3:9091 --topic $2 --property parse.key=true --property key.separator=,
        ;;
    "topic")
        docker run --rm --entrypoint bin/kafka-topics.sh --net=kafka solsson/kafka --zookeeper zookeeper:2181 --create --topic $2 --partitions $3 --replication-factor 1
esac
